grammar Sprinkles;

prog:   ( func )*
        EOF
    ;

func:   'function' ID funcparams
        statementList
        (exit|ret)
        'end'
    ;

funcparams
    :   '(' ')'
    |   '(' ID (',' ID)* ')'
    ;

statementList
    :   (assignmentStatement|funcCall|ifstatement|whilestatement)*
    ;

ifstatement
    :   'if' cond=expr 'then' sl=statementList 'end'
    |   'if' cond=expr 'then' sl=statementList 'else' sl2=statementList 'end'
    ;
	
whilestatement
	: 'while' cond=expr  'do' sl=statementList 'end'
	;

ret :   'return' expr
    ;

exit:   'exit' expr
    ;

assignmentStatement
    :   ID '=' val=expr
    ;

funcCall
    :   ID '(' args=exprList? ')'
    ;

exprList
    :   expr (',' expr)*
    ;

expr:   '(' expr ')'
    |   op='!' right=expr
    |   left=expr op=('+'|'-'|'&'|'|'|'>'|'>='|'<'|'<=') right=expr
    |   left=expr op='==' right=expr
    |   left=expr op='!=' right=expr
    |   funcCall
    |   var=ID
    |   num=INT
    |   neg='-' right=expr
    ;    

INT :   DIGIT+
    ;

ID  :   '.' (LETTER|'_'|'.') (LETTER|DIGIT|'_'|'.')*
    |   LETTER (LETTER|DIGIT|'_'|'.')*
    ;

WS  :   [ \t\n\r]+ -> skip ;

COMMENT:
    '//' .*? '\n' -> skip ;

fragment
DIGIT:  '0'..'9';
fragment
LETTER: [a-zA-Z] ;
