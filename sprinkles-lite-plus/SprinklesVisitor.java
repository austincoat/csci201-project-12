// Generated from Sprinkles.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SprinklesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SprinklesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(SprinklesParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc(SprinklesParser.FuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#funcparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncparams(SprinklesParser.FuncparamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#statementList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementList(SprinklesParser.StatementListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#ifstatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfstatement(SprinklesParser.IfstatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#ret}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(SprinklesParser.RetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#exit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExit(SprinklesParser.ExitContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#assignmentStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentStatement(SprinklesParser.AssignmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#funcCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncCall(SprinklesParser.FuncCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(SprinklesParser.ExprListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SprinklesParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SprinklesParser.ExprContext ctx);
}