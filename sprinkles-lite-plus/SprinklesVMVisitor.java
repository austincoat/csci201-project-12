
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashMap;

public class SprinklesVMVisitor extends SprinklesBaseVisitor<String> implements SprinklesVisitor<String> {
	private int myCounter = 0;
	private HashMap<String,Integer> locals;
	private Integer localCounter;

	public SprinklesVMVisitor() {
		super();
	}

	public String visitProg(SprinklesParser.ProgContext ctx) {
		StringBuilder sb = new StringBuilder();
		for(SprinklesParser.FuncContext func : ctx.func()) {
			sb.append(this.visit(func) + "\n");
		}
		return sb.toString();
	}

	public String visitFunc(SprinklesParser.FuncContext ctx) {
		StringBuilder sb = new StringBuilder();

		locals = new HashMap<String, Integer>();
		localCounter = new Integer(0);
		for(TerminalNode param : ctx.funcparams().ID()) {
			locals.put(param.getText(), localCounter);
			localCounter++;
		}

		String insideCode = this.visit(ctx.statementList());
		if(ctx.exit() != null) { insideCode += this.visit(ctx.exit()); }
		if(ctx.ret() != null) { insideCode += this.visit(ctx.ret()); }
		
		sb.append("function " + ctx.ID().getText() + " " + localCounter + "\n");
		int i = 0;
		for(TerminalNode param : ctx.funcparams().ID()) {
			sb.append("push argument " + i + "\n");
			sb.append("pop local " + locals.get(param.getText()) + "\n");
			i++;
		}
		sb.append(insideCode);

		return sb.toString();
	}

	public String visitStatementList(SprinklesParser.StatementListContext ctx) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < ctx.getChildCount(); i++) {
			sb.append(this.visit(ctx.getChild(i)));
		}
		return sb.toString();
	}

	public String visitAssignmentStatement(SprinklesParser.AssignmentStatementContext ctx) {

		// a variable appeared: ctx.ID().getText()
		if(!locals.containsKey(ctx.ID().getText())) {
			locals.put(ctx.ID().getText(), localCounter);
			localCounter++;
		}
		return (this.visit(ctx.val) + "pop local " + locals.get(ctx.ID().getText()) + " // " + ctx.ID().getText() + "\n");
	}

	public String visitFuncCall(SprinklesParser.FuncCallContext ctx) {
		StringBuilder sb = new StringBuilder();
		String funcname = ctx.ID().getText();
		int argCount = 0;
		if(ctx.args != null) {
			for(SprinklesParser.ExprContext expr : ctx.args.expr()) {
				sb.append(this.visit(expr));
				argCount++;
			}
		}

		sb.append("call " + funcname + " " + argCount + "\n");
		return sb.toString();
	}

    public String visitIfstatement(SprinklesParser.IfstatementContext ctx) {
        StringBuilder sb = new StringBuilder();
		if(ctx.sl2 == null)
		{
			sb.append(this.visit(ctx.cond));
			sb.append("not\n");
			myCounter++;
			String label = "LABEL" + myCounter;
			sb.append("if-goto " + label + "\n");
			sb.append(this.visit(ctx.sl));
			sb.append("label " + label + "\n");
		}
		else
		{
			sb.append(this.visit(ctx.cond));
			sb.append("not\n");
			myCounter++;
			String myLabel = "Kitty" + myCounter;
			myCounter++;
			sb.append("if-goto " + myLabel + "\n");
			sb.append(this.visit(ctx.sl2));
			String myLabel2 = "Kitty" + myCounter;
			sb.append("goto " + myLabel2 + "\n" );
			sb.append("label " + myLabel + "\n" );
			sb.append(this.visit(ctx.sl));
			sb.append("label " + myLabel2 + "\n")
		}
        return sb.toString();
    }

	public String visitWhileStatement(SprinklesParser.WhilestatementContext ctx)
	{
		StringBuilder sb = new StringBuilder();
		myCounter++;
		sb.append("label loop" + myCounter + "\n");
		sb.append(this.visit(ctx.cond));
		sb.append("not \n");
		myCounter++;
		String myLabel = "Puppy" + myCounter;
		sb.append("if-goto Puppy" + myCounter + "\n")
		sb.append(this.visit(ctx.sl));
		sb.append("goto loop" + (myCounter-1) + "\n")
		sb.append("Puppy" + myLabel + "\n")
		return sb.toString();


	}

	public String visitExpr(SprinklesParser.ExprContext ctx) {
		StringBuilder sb = new StringBuilder();
		if(ctx.op != null) {
			if(ctx.op.getText().equals("!")) {
				sb.append(this.visit(ctx.right));
				sb.append("not\n");
			}
			else if(ctx.op.getText().equals("+")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("add\n");
			}
			else if(ctx.op.getText().equals("-")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("sub\n");
			}
			else if(ctx.op.getText().equals("==")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("eq\n");
			}
			else if(ctx.op.getText().equals("!=")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("eq\n");
				sb.append("not\n");
			}
			else if(ctx.op.getText().equals("&")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("and")
			}
			else if(ctx.op.getText().equals("|")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("or")
			}
			else if(ctx.op.getText().equals(">")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("gt")
			}
			else if(ctx.op.getText().equals(">=")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("gt")
				sb.append("not")
			}
			else if(ctx.op.getText().equals("<")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("lt")
			}
			else if(ctx.op.getText().equals("<=")) {
				sb.append(this.visit(ctx.left));
				sb.append(this.visit(ctx.right));
				sb.append("lt")
				sb.append("not")
			}
		}
		else if(ctx.neg != null) {
			sb.append(this.visit(ctx.right));
			sb.append("neg\n");
		}
		else if(ctx.num != null) {
			sb.append("push constant " + ctx.num.getText() + "\n");
		}
		else if(ctx.var != null) {
			if(!locals.containsKey(ctx.var.getText())) {
				locals.put(ctx.var.getText(), localCounter);
				localCounter++;
			}
			sb.append("push local " + locals.get(ctx.var.getText()) + "\n");
		}
		else if(ctx.funcCall() != null) {
			sb.append(this.visit(ctx.funcCall()));
		}
		else if(ctx.expr() != null) { // nested expression in parens
			for(SprinklesParser.ExprContext expr : ctx.expr()) {
				sb.append(this.visit(expr));
			}
		}
		return sb.toString();
	}

	public String visitRet(SprinklesParser.RetContext ctx) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.visit(ctx.expr()));
		sb.append("return\n");
		return sb.toString();
	}

	public String visitExit(SprinklesParser.ExitContext ctx) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.visit(ctx.expr()));
		sb.append("label __end__\n");
		sb.append("goto __end__\n");
		return sb.toString();
	}
}
