import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Sprinkles {
	public static void main(String[] args) throws Exception {
		String inputFile = args[0];
		InputStream is = new FileInputStream(inputFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		ANTLRInputStream input = new ANTLRInputStream(br);
		SprinklesLexer lexer = new SprinklesLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SprinklesParser parser = new SprinklesParser(tokens);
		SprinklesParser.ProgContext tree = parser.prog();
		String vmcode = new SprinklesVMVisitor().visit(tree);
		System.out.println(vmcode);
	}
}
